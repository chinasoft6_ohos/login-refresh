# 登录界面，列表下拉和加载操作

#### 项目介绍

- 项目名称：LoginRefresh
- 功能：1，完整的登录页面和逻辑 2，列表的下拉刷新和上拉加载更多。

#### 效果演示

![](gif/refresh1.gif)
![](gif/refresh2.gif)
![](gif/refresh3.gif)

#### 安装环境

在sdk6，DevEco Studio2.2 Beta1 下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件
， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 项目目录
![](gif/page.png)

- index 是登录页面文件（css,hml,js）
- listpage 是列表页面，包括刷新和加载更多

#### 组件接口

##### 登录页面组件
- text：标题/注册页入口
- input：账号/密码 输入框
- button：登录按钮

##### 事件
change：监听账号输入框的数据变化
change2：监听密码输入框的数据变化
loginClick：点击登录按钮触发事件


##### 列表页面组件
- input：查询输入框
- list：列表组件父类
- list-item：列表item子类
- image：图片

##### 事件
touchstartfunc：触摸事件手指点击事件
touchmovefunc：触摸事件手指滑动事件
touchendfunc：触摸事件手指抬起事件
scrollbottomfunc：列表滚动到底部事件
scrolltopfunc：列表滚动到头部事件

#### 功能逻辑
- 登录页面

1.hml布局页面
```xml
    <div class="container">
        <text class="title">
            {{ $t('strings.title') }}
        </text>
    
        <div class="loginlayout">
            <text class="login-title">Please Login</text>
    
            <input id="username" class="input" type="text" value="" maxlength="20"
                   enterkeytype="send" placeholder="请输入账号" onchange="change"
                   onenterkeyclick="enterkeyClick">
            </input>
    
            <input id="password" class="input" type="text" value="" maxlength="10"
                   enterkeytype="send" placeholder="请输入密码" onchange="change2"
                    >
            </input>
    
            <button class="button" onclick="loginClick">点击登录</button>
        </div>
    
        <div class="bottom">
        <text class="bottomtext">Don't have an account? </text> <text class="acontent" href="#">Register</text>
        </div>
    </div>
```

2.页面样式
```xml
    .container {
        flex-direction: column;
        align-items: center;
        align-content: center;
        background-color: steelblue;
    }
    
    .loginlayout {
        flex-direction: column;
        align-items: center;
        align-content: center;
        margin-top: 20%;
        width: 90%;
        background-color: lightblue;
        padding: 20px;
        border-radius: 5px;
    }
    
    .title {
        font-size: 20px;
        color: #000000;
        opacity: 0.9;
        width: 100%;
        color: aliceblue;
        background-color: black;
        height: 50px;
        text-align: center;
        font-weight: 700;
    }
    
    .input {
        placeholder-color: gray;
        width: 100%;
        margin-bottom: 20px;
        margin-top: 20px;
    }
    .button {
        width: 100%;
        background-color: cadetblue;
        padding: 10px;
        font-family: inherit;
        font-size: 16px;
        border: 0;
        border-radius: 5px;
    }
    
    .button:active {
        transform: scale(0.98);
    }
    
    .button:focus {
        outline: 0;
    }
    .bottomtext{
        font-size: 18px;
    }
    
    .acontent{
        font-size: 18px;
        text-decoration: none;
        margin-left: 5px;
        color: lightblue;
    }
    
    .bottom{
        flex-direction: row;
        justify-content: center;
        width: 100%;
        margin-top: 20px;
    }

```
3.页面功能操作逻辑
```xml
import router from '@system.router';
import prompt from '@system.prompt'

export default {
    data: {
        title: "",
        username: "",
        password: ""
    },
    onInit() {
        this.title = this.$t('strings.title');
        router.push({
            uri:'pages/imagepage/imagepage'
        })
    },

    change(e){
        this.username = e.value;
        //        prompt.showToast({
        //            message: "value 1: " + this.username,
        //            duration: 3000,
        //        });
    },
    change2(e){
        this.password = e.value;
    },
    enterkeyClick(e){
        prompt.showToast({
            message: "enterkey clicked",
            duration: 3000,
        });
    },

    loginClick(e){
        //    this.$element("input").showError({
        //      error: '请输入正确信息'
        //    });

        prompt.showToast({
            message: '登录成功！',
            duration: 2000,
        });

        router.push({
            uri: "pages/listpage/listpage",
            params: this.username +"-"+this.password
        })
    },
}
```

- 列表页面

1.hml布局页面
```xml
<div class="container">
    <text class="title">
        {{ $t('strings.listTitle') }}
    </text>
    <div class="search">
        <input type="text" id="filter" placeholder="Search"/>
    </div>

    <div class="refresh-status" style="height : {{ refreshhight }} px">
        <text>{{ refreshtitle }}</text>
    </div>

    <div class="content-list" @touchstart='touchstartfunc' @touchmove="touchmovefunc"
         @touchend='touchendfunc'>
        <list scrolleffect="no" @scrollbottom="scrollbottomfunc" @scrolltop="scrolltopfunc" @scroll="scrollfunc" >
            <list-item for="(index,item) in contentlist">
                <div class="all-show">
                    <div class="row-show">
                        <image class="image-con" src="common/images/avatar{{ index }}.jpg"></image>
                        <div class="column-show">
                            <text class="text1">{{ item.title }}</text>
                            <text class="text2">{{ item.detail }}</text>
                        </div>
                    </div>
                    <div class="line-content"></div>
                </div>
            </list-item>
        </list>
    </div>
    <div id="button" class="load-more-status" style="height: {{loadhight}};" >
        <text>加载更多</text>
    </div>
</div>

```
2.页面样式
```xml
.container {
    display: flex;
    flex-direction: column;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
}

.title {
    font-size: 18px;
    color: #000000;
    opacity: 0.9;
    width: 100%;
    color: aliceblue;
    background-color: black;
    height: 50px;
    text-align: center;
    font-weight: 700;
    flex-shrink: 0;
}

.search {
    background-color: darkgrey;
    color: #ffffff;
    padding: 10px 10px;
    flex-shrink: 0;
}

.search input {
    background-color: rgba(0, 0, 0, 0.3);
    color: #ffffff;
    font-size: 16px;
    padding: 0px 15px;
    width: 100%;
}

.image-con {
    width: 50px;
    height: 50px;
    border-radius: 25px;
}

.content-list {
    height: 100%;
}

.column-show {
    flex-direction: column;
    align-items: center;
    margin-left: 5px;
    padding: 5px;
}

.all-show {
    flex-direction: column;
    justify-content: center;
    align-content: center;
    align-items: center;
}

.row-show {
    padding: 15px;
}

.text1 {
    font-size: 18px;
    color: black;
    text-align: left;
}

.text2 {
    font-size: 13px;
    color: darkgray;
    margin-top: 5px;
}

.line-content {
    width: 100%;
    height: 1px;
    background-color: darkgrey;
}

.refresh-status {
    background-color: red;
    justify-content: center;
    width: 100%;
}

.refresh-status text {
    font-size: 16px;
}

.load-more-status {
    background-color: red;
    justify-content: center;
    width: 100%;
    height: 0px;
}

.load-more-status text {
    font-size: 16px;
}

.animation {
    animation: simpleFrames 1s 200ms;
}

/*动画*/
@keyframes simpleFrames {
    from {
        transform: scale(1);
        opacity: 0;
        height: 50px;
    }
    to {
        transform: scale(0);
        opacity: 1;
        height: 0px;
    }
}
```
3.页面功能操作逻辑
```xml
import router from '@system.router';

export default {
    data: {
        refreshhight: 0,
        listtitle: '',
        refreshtitle: '下拉刷新',
        loadhight: 0,
        startheight: 0,
        touchheight: 0,
        playState: '',
        contentlist: [{
                          title: 'Rasmus Saari',
                          detail: 'Muhos, Finland',
                      }, {
                          title: 'Victoria Barnaby',
                          detail: 'Fountainbleu, Canada',
                      }, {
                          title: 'Anton Justi',
                          detail: 'Ulvila, Finland',
                      }, {
                          title: 'Gudula Bätz',
                          detail: 'Roth, Germany',
                      }, {
                          title: 'Lucas Brown',
                          detail: 'Vanier, Canada',
                      }, {
                          title: 'Dyonne De Roo',
                          detail: 'Hekelingen, Netherlands',
                      }, {
                          title: 'Rasmus Saari',
                          detail: 'Muhos, Finland',
                      }, {
                          title: 'Victoria Barnaby',
                          detail: 'Fountainbleu, Canada',
                      }, {
                          title: 'Anton Justi',
                          detail: 'Ulvila, Finland',
                      }, {
                          title: 'Gudula Bätz',
                          detail: 'Roth, Germany',
                      }],
    },
    onInit() {
        this.title = this.$t('strings.listtitle');
    },
    touchstartfunc(msg) {
        console.info(`on touch start, touchstart is: ${msg.touches[0].globalY}`);
        this.startheight = msg.touches[0].globalY;
    },
    touchmovefunc(msg) {
        console.info(`on touch start, touchmove is: ${msg.touches[0].globalY}`);
        let mavegeight = msg.touches[0].globalY - this.startheight;
        this.touchheight = mavegeight;
        console.info('===== mavegeight = ' + mavegeight)
        if (mavegeight > 1 && mavegeight <= 250) {
            this.refreshtitle = "下拉刷新";
        } else {
            this.refreshtitle = "松手刷新";
        }
        if (mavegeight > 10 && mavegeight <= 400) {
            this.refreshhight = mavegeight;
        }
    },
    touchendfunc(msg) {
        // 刷新的时候 松手，操作网络接口 并且 隐藏刷新效果
        if (this.touchheight > 10) {
            if (this.touchheight < 250) {
                this.refreshhight = 0;
                return;
            }

            this.refreshhight = 100;
            console.info('this.touchendfunc')
            this.refreshtitle = "刷新中...";
            // 3秒后隐藏刷新
            setTimeout(() => {
                this.refreshhight = 0;
            }, 2000);
        }
    },
    scrollbottomfunc(msg) { // 滑动到底部 加载更多
        this.loadhight = 50;
        let button = this.$element('button');
        var timeoutID = setTimeout(() => {
            this.loadhight = 0;
            button.scrollTo(10);
            clearTimeout(timeoutID);
        }, 2000);
    },
    scrolltopfunc(msg) {// 滑动到顶部
//        console.info(`on touch start, touchmove is: ${msg.touches[0].globalY}`);
        this.loadhight = 0;
    },
    scrollfunc(msg) {// 滑动
         console.info(`scrollfunc: ${msg.y}`);
    }
}

```