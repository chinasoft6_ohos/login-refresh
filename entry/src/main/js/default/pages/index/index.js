import router from '@system.router';
import prompt from '@system.prompt'

export default {
    data: {
        title: "",
        username: "",
        password: ""
    },
    onInit() {
        this.title = this.$t('strings.title');
        router.push({
            uri:'pages/imagepage/imagepage'
        })
    },

    // 监听用户输入框中数据信息的变化
    change(e){
        this.username = e.value;
    },
    // 监听密码输入框中数据信息的变化
    change2(e){
        this.password = e.value;
    },
    /**
     * 点击输入框操作
     *
     * @param e
     * @return 点击输入框操作
     */
    enterkeyClick(e){
        prompt.showToast({
            message: "enterkey clicked",
            duration: 3000,
        });
    },
    /**
     * 登录点击操作
     */
    loginClick(e){
        //    this.$element("input").showError({
        //      error: '请输入正确信息'
        //    });

        prompt.showToast({
            message: '登录成功！',
            duration: 2000,
        });

        router.push({// 页面跳转进入列表刷新
            uri: "pages/listpage/listpage",
            params: this.username +"-"+this.password
        })
    },
}
